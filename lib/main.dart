import 'package:flutter/material.dart';

void main() => runApp(HelloFlutterApp());
class HelloFlutterApp extends StatefulWidget {
  @override
  _HelloFlutterAppState createState() => _HelloFlutterAppState();
}
String englishGreeting = "Hello Flutter";
String spanishGreeting = "Hola Flutter";
String russianGreeting = "привет флаттер";
String frenchGreeting = "Bonjour Flutter";
class _HelloFlutterAppState extends State<HelloFlutterApp>{
  String displayText = englishGreeting;

  @override
  Widget build(BuildContext context){
    return MaterialApp(
      debugShowCheckedModeBanner:  false,
      home: scafford(
        appBar: Appbar(
          title: Text("Hello Flutter"),
          leading: Icon(Icon.home),
          actions: <Widget>[
            IconButton(
                onPressed:() {
                  setState((){
                    displayText = displayText == englishGreeting?
                    spanishGreeting:englishGreeting;
                  })
                },
                icon: Icon(Icon.refresh))
            IconButton(
                onPressed:() {
                  setState((){
                    displayText = displayText == englishGreeting?
                    russianGreeting:englishGreeting;
                  })
                },
                icon: Icon(Icon.refresh))
            IconButton(
                onPressed:() {
                  setState((){
                    displayText = displayText == englishGreeting?
                    frenchGreeting:englishGreeting;
                  })
                },
                icon: Icon(Icon.refresh))
          ]
        ),
        body: Center(
          child: Text(displayText,
            style:TextStyle(fontSize: 24),
          ),
          )
        )
      ),
    );
  }


}